package com.project.restapiwithjpa.Controller;

import com.project.restapiwithjpa.Repository.StudentCourse;
import com.project.restapiwithjpa.Service.StudentCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/studentCourse")
public class StudentCourseController {

    @Autowired
    private StudentCourseService studentCourseService;

    @PostMapping("/enroll")
    public StudentCourse enroll(@RequestBody StudentCourse studentCourse) {
        return studentCourseService.save(studentCourse);
    }

    @GetMapping("/getAllEnrolled")
    public List<StudentCourse> getAllEnrolled() {
        return studentCourseService.findAll();
    }
}
