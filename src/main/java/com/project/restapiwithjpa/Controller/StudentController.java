package com.project.restapiwithjpa.Controller;

import com.project.restapiwithjpa.Repository.Course;
import com.project.restapiwithjpa.Repository.Student;
import com.project.restapiwithjpa.Repository.StudentCourse;
import com.project.restapiwithjpa.Service.StudentCourseService;
import com.project.restapiwithjpa.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentCourseService studentCourseService;

    @PostMapping("/addStudent")
    public Student addStudent(@RequestBody Student student){
        return studentService.save(student);
    }

    @GetMapping("/getAllStudents")
    public List<Student> getAllStudents(){
        return studentService.findAll();
    }

    @GetMapping("/getById/{id}")
    public Student getById(@PathVariable long id){
        return studentService.findById(id);
    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable long id){
        studentService.deleteById(id);
    }

    @PutMapping("/updateById/{id}")
    public Student updateById(@PathVariable long id, @RequestBody Student student){
        return studentService.updateById(id, student);
    }

    @PostMapping("/enroll/{id}")
    public StudentCourse enroll(@PathVariable long id, @RequestBody Course course){
        Student student = studentService.findById(id);

        StudentCourse studentCourse = new StudentCourse();
        studentCourse.setStudent(student);
        studentCourse.setCourse(course);
        return studentCourseService.save(studentCourse);
    }

    @DeleteMapping("/drop/{id}")
    public void drop(@PathVariable long id, @RequestBody Course course){
        studentCourseService.deleteByStudentAndCourse(id, course);
    }

}
