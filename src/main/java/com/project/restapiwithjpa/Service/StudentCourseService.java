package com.project.restapiwithjpa.Service;

import com.project.restapiwithjpa.Repository.Course;
import com.project.restapiwithjpa.Repository.StudentCourse;

import java.util.List;

public interface StudentCourseService {

    StudentCourse save(StudentCourse studentCourse);

    List<StudentCourse> findAll();

    StudentCourse findById(long id);

    void deleteById(long id);

    StudentCourse updateById(long id, StudentCourse studentCourse);

    void deleteByStudentAndCourse(long studentId, Course course);

}
