package com.project.restapiwithjpa.Service;

import com.project.restapiwithjpa.Exception.BadRequestException;
import com.project.restapiwithjpa.Repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentCourseServiceImplementation implements StudentCourseService {

    @Autowired
    StudentCourseRepository studentCourseRepository;

    @Autowired
    StudentRepository studentRepository;


    @Override
    public StudentCourse save(StudentCourse studentCourse) {
        return studentCourseRepository.save(studentCourse);
    }

    @Override
    public List<StudentCourse> findAll() {
        return studentCourseRepository.findAll();
    }

    @Override
    public StudentCourse findById(long id) {
        return studentCourseRepository.findById(id).orElseThrow(() -> new BadRequestException("Student_Course does not exist"));
    }

    @Override
    public void deleteById(long id) {
        studentCourseRepository.deleteById(id);
    }

    @Override
    public StudentCourse updateById(long id, StudentCourse studentCourse) {
        studentCourse.setId(id);
        return studentCourseRepository.save(studentCourse);
    }

    public void deleteByStudentAndCourse(long studentId, Course course) {
        Student student = studentRepository.findById(studentId)
                .orElseThrow(() -> new BadRequestException("Student with id " + studentId + " not found"));

        StudentCourse studentCourse = studentCourseRepository.findByStudentAndCourse(student, course)
                .orElseThrow(() -> new BadRequestException("Student is not enrolled in this course"));

        studentCourseRepository.delete(studentCourse);
    }
}
