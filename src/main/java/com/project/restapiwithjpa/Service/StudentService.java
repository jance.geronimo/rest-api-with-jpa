package com.project.restapiwithjpa.Service;

import com.project.restapiwithjpa.Repository.Student;

import java.util.List;

public interface StudentService {

    Student save(Student student);

    List<Student> findAll();

    Student findById(long id);

    void deleteById(long id);

    Student updateById(long id, Student student);
}
