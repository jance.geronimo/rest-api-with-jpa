package com.project.restapiwithjpa.Service;

import com.project.restapiwithjpa.Repository.Course;

import java.util.List;

public interface CourseService {

    Course save(Course course);

    List<Course> findAll();

    Course findById(long id);

    void deleteById(long id);

    Course updateById(long id, Course course);
}
