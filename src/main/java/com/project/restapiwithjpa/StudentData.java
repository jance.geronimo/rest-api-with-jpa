package com.project.restapiwithjpa;

import com.project.restapiwithjpa.Repository.Course;
import com.project.restapiwithjpa.Repository.Student;
import com.project.restapiwithjpa.Repository.StudentCourse;
import com.project.restapiwithjpa.Service.CourseService;
import com.project.restapiwithjpa.Service.StudentCourseService;
import com.project.restapiwithjpa.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class StudentData implements CommandLineRunner {
    @Autowired
    private StudentService studentService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private StudentCourseService studentCourseService;

    private final ArrayList<Course> defaultCourseData = new ArrayList<>();
    private final ArrayList<Student> defaultStudentData = new ArrayList<>();

    @Override
    public void run(String... args) throws Exception {
        populateCourseTable();
        populateStudentTable();
        populateStudentCourseTable();
    }

    private void populateCourseTable() {
        Course course1 = new Course();
        course1.setName("Introduction to Filmmaking");
        course1.setDescription("This course provides a comprehensive overview of the filmmaking process, including pre-production, production, and post-production.");
        courseService.save(course1);
        defaultCourseData.add(course1);

        Course course2 = new Course();
        course2.setName("Cinematography");
        course2.setDescription("This course focuses on the art and science of cinematography. Students learn about camera operation, lighting techniques, composition, and visual storytelling.");
        courseService.save(course2);
        defaultCourseData.add(course2);

        Course course3 = new Course();
        course3.setName("Screenwriting");
        course3.setDescription("This course delves into the craft of screenwriting. Students learn the fundamentals of storytelling, character development, dialogue writing, and screenplay formatting.");
        courseService.save(course3);
        defaultCourseData.add(course3);

        Course course4 = new Course();
        course4.setName("Film Editing");
        course4.setDescription("This course explores the principles of film editing and post-production techniques. Students learn about the editing process, using industry-standard software, and enhancing the narrative flow of a film.");
        courseService.save(course4);
        defaultCourseData.add(course4);

        Course course5 = new Course();
        course5.setName("Directing");
        course5.setDescription("This course covers the essential skills and techniques required to become a film director. Students learn about shot composition, blocking, working with actors, and visual storytelling.");
        courseService.save(course5);
        defaultCourseData.add(course5);

        Course course6 = new Course();
        course6.setName("Documentary Filmmaking");
        course6.setDescription("This course focuses on creating non-fiction films. Students learn about documentary research, storytelling, interviewing techniques, and capturing real-life stories.");
        courseService.save(course6);
        defaultCourseData.add(course6);

        Course course7 = new Course();
        course7.setName("Sound Design");
        course7.setDescription("This course explores the role of sound in film and how it contributes to the cinematic experience. Students learn about sound recording, editing, mixing, and creating immersive soundscapes.");
        courseService.save(course7);
        defaultCourseData.add(course7);

        Course course8 = new Course();
        course8.setName("Production Design");
        course8.setDescription("This course delves into the visual and aesthetic aspects of filmmaking. Students learn about production design principles, set construction, and creating cohesive visual worlds for films.");
        courseService.save(course8);
        defaultCourseData.add(course8);

        Course course9 = new Course();
        course9.setName("Film Marketing and Distribution");
        course9.setDescription("This course covers the business side of filmmaking. Students learn about film marketing strategies, distribution models, film festivals, and navigating the film industry.");
        courseService.save(course9);
        defaultCourseData.add(course9);

        Course course10 = new Course();
        course10.setName("Film Theory and Analysis");
        course10.setDescription("This course examines film as an art form and explores various theories and approaches to film analysis. Students learn about film genres, cinematic techniques, and interpreting films.");
        courseService.save(course10);
        defaultCourseData.add(course10);
    }

    private void populateStudentTable() {
        Student student1 = new Student();
        student1.setFirstName("Tyler");
        student1.setLastName("Durden");
        student1.setEmail("tylerdurden@gmail.com");
        studentService.save(student1);

        Student student2 = new Student();
        student2.setFirstName("Patrick");
        student2.setLastName("Bateman");
        student2.setEmail("patrickbateman@gmail.com");
        studentService.save(student2);

        Student student3 = new Student();
        student3.setFirstName("Travis");
        student3.setLastName("Bickle");
        student3.setEmail("travisbickle@yahoo.com");
        studentService.save(student3);

        Student student4 = new Student();
        student4.setFirstName("Walter");
        student4.setLastName("White");
        student4.setEmail("heisenberg@outlook.com");
        studentService.save(student4);

        Student student5 = new Student();
        student5.setFirstName("Donnie");
        student5.setLastName("Darko");
        student5.setEmail("donniedarko@aol.com");
        studentService.save(student5);

        Student student6 = new Student();
        student6.setFirstName("Kentaro");
        student6.setLastName("Miura");
        student6.setEmail("kentaromiura@protonmail.com");
        studentService.save(student6);

        Student student7 = new Student();
        student7.setFirstName("Johan");
        student7.setLastName("Libert");
        student7.setEmail("monster@icloud.com");
        studentService.save(student7);

        Student student8 = new Student();
        student8.setFirstName("Musashi");
        student8.setLastName("Miyamoto");
        student8.setEmail("musashimiyamoto@zoho.com");
        studentService.save(student8);

        Student student9 = new Student();
        student9.setFirstName("Thorfinn");
        student9.setLastName("Karlsefni");
        student9.setEmail("thorfinn@mail.com");
        studentService.save(student9);

        Student student10 = new Student();
        student10.setFirstName("Spike");
        student10.setLastName("Spiegel");
        student10.setEmail("spikespiegel@gmx.com");
        studentService.save(student10);

        defaultStudentData.addAll(studentService.findAll());
    }

    private void populateStudentCourseTable() {
        StudentCourse studentCourse1 = new StudentCourse();
        studentCourse1.setStudent(defaultStudentData.get(0));
        studentCourse1.setCourse(defaultCourseData.get(0));
        studentCourseService.save(studentCourse1);

        // student 2 enrolls in course 6
        StudentCourse studentCourse2 = new StudentCourse();
        studentCourse2.setStudent(defaultStudentData.get(1));
        studentCourse2.setCourse(defaultCourseData.get(5));
        studentCourseService.save(studentCourse2);

        // student 3 enrolls in course 9
        StudentCourse studentCourse3 = new StudentCourse();
        studentCourse3.setStudent(defaultStudentData.get(2));
        studentCourse3.setCourse(defaultCourseData.get(8));
        studentCourseService.save(studentCourse3);

        // student 4 enrolls in course 1 and course 5
        StudentCourse studentCourse4a = new StudentCourse();
        studentCourse4a.setStudent(defaultStudentData.get(3));
        studentCourse4a.setCourse(defaultCourseData.get(0));
        studentCourseService.save(studentCourse4a);

        StudentCourse studentCourse4b = new StudentCourse();
        studentCourse4b.setStudent(defaultStudentData.get(3));
        studentCourse4b.setCourse(defaultCourseData.get(4));
        studentCourseService.save(studentCourse4b);

        // student 5 has no course


        // student 6 enrolls in course 6
        StudentCourse studentCourse6 = new StudentCourse();
        studentCourse6.setStudent(defaultStudentData.get(5));
        studentCourse6.setCourse(defaultCourseData.get(5));
        studentCourseService.save(studentCourse6);

        // student 7 enrolls in course 7 and course 8

        StudentCourse studentCourse7a = new StudentCourse();
        studentCourse7a.setStudent(defaultStudentData.get(6));
        studentCourse7a.setCourse(defaultCourseData.get(6));
        studentCourseService.save(studentCourse7a);

        StudentCourse studentCourse7b = new StudentCourse();
        studentCourse7b.setStudent(defaultStudentData.get(6));
        studentCourse7b.setCourse(defaultCourseData.get(7));
        studentCourseService.save(studentCourse7b);

        // student 8 enrolls in course 10
        StudentCourse studentCourse8 = new StudentCourse();
        studentCourse8.setStudent(defaultStudentData.get(7));
        studentCourse8.setCourse(defaultCourseData.get(9));
        studentCourseService.save(studentCourse8);

        // student 9 has no course

        // student 10 enrolls in course 5
        StudentCourse studentCourse10 = new StudentCourse();
        studentCourse10.setStudent(defaultStudentData.get(9));
        studentCourse10.setCourse(defaultCourseData.get(4));
        studentCourseService.save(studentCourse10);
    }

}
