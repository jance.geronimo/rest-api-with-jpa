package com.project.restapiwithjpa.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StudentCourseRepository extends JpaRepository<StudentCourse, Long> {
    Optional<StudentCourse> findByStudentAndCourse(Student student, Course course);
}
